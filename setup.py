#!/usr/bin/env python2
from setuptools import setup, find_packages
import os

version = __import__('chameleon').get_version()
here = os.path.dirname(os.path.abspath(__file__))

## Get long_description from README:
f = open(os.path.join(here, 'README.rst'))
long_description = f.read().strip()
f.close()
f = open(os.path.join(here, 'CHANGELOG.rst'))
long_description += "\n\n" + f.read()
f.close()


setup(
    name = "django-chameleon",
    version = version,
    url = 'https://bitbucket.org/oddotterco/django-chameleon',
    license = 'New BSD License',
    platforms=['any'],
    description = "A Django app that will create a pluggable django-cms theme app within your project.",
    long_description = long_description,
    keywords='django, cms, theme',
    author = "Odd Otter Co",
    author_email = 'chameleon@oddotter.com',
    packages = find_packages(),
    install_requires = [
        'Django>=1.4',
        'django-cms>=2.3.5',
        'django-compressor',
        ],
    dependency_links = [],
    include_package_data = True,
    zip_safe = False,
    classifiers = [
        'Development Status :: 3 - Alpha',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],

    entry_points = {
        'console_scripts': [
            'purifyhtml = chameleon.management.commands.purifyhtml:execute',
        ],
    },
)
