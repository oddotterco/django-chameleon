from django.conf import settings
gettext = lambda s: s

VERSION = (0,0,0,'alpha',1)
def get_version(version=None):
    """Derives a PEP386-compliant version number from VERSION."""
    if version is None:
        version = VERSION
    assert len(version) == 5
    assert version[3] in ('alpha', 'beta', 'rc', 'final')

    # Now build the two parts of the version number:
    # main = X.Y[.Z]
    # sub = .devN - for pre-alpha releases
    #     | {a|b|c}N - for alpha, beta and rc releases

    parts = 2 if version[2] == 0 else 3
    main = '.'.join(str(x) for x in version[:parts])

    sub = ''
    if version[3] == 'alpha' and version[4] == 0:
        # At the toplevel, this would cause an import loop.
        from django.utils.version import get_svn_revision
        svn_revision = get_svn_revision()[4:]
        if svn_revision != 'unknown':
            sub = '.dev%s' % svn_revision

    elif version[3] != 'final':
        mapping = {'alpha': 'a', 'beta': 'b', 'rc': 'c'}
        sub = mapping[version[3]] + str(version[4])

    return main + sub

settings.CMS_TEMPLATES += (
    ('{{ app_name }}/{{ app_name }}.html', gettext('Base Template ({{ app_name }})')),
    )

# Enable LESS compile through Django-Compressor
if not 'compressor' in settings.INSTALLED_APPS:
    settings.INSTALLED_APPS += ('compressor',)
if not getattr(settings, 'COMPRESS_OUTPUT_DIR', False):
    settings.COMPRESS_OUTPUT_DIR = 'cache'
if not getattr(settings, 'COMPRESS_CSS_FILTERS', False):
    settings.COMPRESS_CSS_FILTERS = []
if not 'compressor.filters.cssmin.CSSMinFilter' in settings.COMPRESS_CSS_FILTERS:
    settings.COMPRESS_CSS_FILTERS += ('compressor.filters.cssmin.CSSMinFilter',)
if not 'compressor.filters.css_default.CssAbsoluteFilter' in settings.COMPRESS_CSS_FILTERS:
    settings.COMPRESS_CSS_FILTERS += ('compressor.filters.css_default.CssAbsoluteFilter',)
if not 'compressor.filters.template.TemplateFilter' in settings.COMPRESS_CSS_FILTERS:
    settings.COMPRESS_CSS_FILTERS += ('compressor.filters.template.TemplateFilter',)

if not getattr(settings, 'COMPRESS_PRECOMPILERS', False):
    settings.COMPRESS_PRECOMPILERS = []
    ##TODO: actually check if it already is in the settings file BEFORE ADDING IT!
    settings.COMPRESS_PRECOMPILERS += (
        ('text/less', 'lessc {infile} {outfile}'),
        )

try:
    settings.STATICFILES_FINDERS
except NameError:
    settings.STATICFILES_FINDERS = []
if not 'compressor.finders.CompressorFinder' in settings.STATICFILES_FINDERS:
    settings.STATICFILES_FINDERS += (
        'compressor.finders.CompressorFinder',
        )
