django-chameleon
****************

.. django-chameleon:: A Django app that will create a pluggable django-cms theme app within your project..

Dependencies
============

- django
- django-cms

Getting Started
=============

TBD

Usage
=====

TBD

Production Environment
======================

TBD
